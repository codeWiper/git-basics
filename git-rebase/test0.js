function Car(make, year) {
    this.make = make;
    this.year = year;
}

var car1 = new Car('maruti', 1990);

function print(car) {
    console.log(car.make + car.year);
}